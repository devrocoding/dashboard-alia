<div class="code-area">
    <span style="color: #777;font-style:italic;">
      // 400 user not found.
    </span>
    <span>
      <span style="color:#d65562;">
        if
      </span>
      (<span style="color:#4ca8ef;">!</span><span style="font-style: italic;color:#bdbdbd;">found</span>)
      {
      </span>
    <span>
        <span style="padding-left: 15px;color:#2796ec">
          <i style="width: 10px;display:inline-block"></i>echo
        </span>
        <span>
          "<span style="color: #a6a61f">Stop searching for numbers!</span>";
        </span>
        <br>
        <span style="padding-left: 15px;color:#2796ec">
          <i style="width: 10px;display:inline-block"></i>throw
        </span>
        <span>
          new (<span style="color: #a6a61f">peopleAreHumansNotNumbers()</span>);
        </span>
        <br>
        <span>
        <span style="display:block">}</span>
      </span>
</div>
