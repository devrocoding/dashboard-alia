<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 19:17
 */

require_once '../dist/php/SessionUser.php';
require_once '../dist/php/Database.php';
require_once '../dist/php/Functions.php';
require_once '../dist/php/Role.php';
require_once '../dist/php/MinecraftUUID.php';

$user = new SessionUser();
$db = new Database();
$func = new Functions();
$role = new Role();
$uuidMC = new MinecraftUUID();

$can_log = false;
if ($user->isLoggedIn()) {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
//        var_dump($_POST);
        $db->connect();
        if (isset($_POST['DELETE_USER'])) {
            $id = $_POST['DELETE_USER'];
            $db->delete('user_data', 'id=' . $id);
            echo json_encode("SUCCES");
        }else
            if (isset($_POST['LOGOUT'])) {
                $user->signOut();
                echo json_encode("SUCCES");
            } else
                if (isset($_POST['ADD_USER'])) {
                    $title = $_POST['email'];

                    $db->select('user_data', '*', null, "email='" . $title . "'");
                    if ($db->numRows() > 0) {
                        echo json_encode("This user already exsists. Check the field");
                        return;
                    } else {
                        $name = $_POST['name'];
                        $image = $_POST['image'];
//                        $payment = $_POST['payment'];
                        $minecraft = $_POST['minecraft'];
                        $uuid = $uuidMC->format_uuid($uuidMC->username_to_profile($minecraft)['id']);
                        $roleName = $role->formatRoleID(strtoupper($_POST['role']));

                        $db->insert('user_data', array('id' => null, 'email' => $title, 'password' => null, 'minecraft_name' => $minecraft, 'role' => $roleName,
                                'name' => $name, 'image' => $image, 'minecraft_uuid'=> $uuid /* ,'payment' => $payment */, 'date_added' => date('Y-m-d'), 'server_owner' => null));

                        echo json_encode("Succes, we saved the new user");
                    }
                }else

                    if (isset($_POST['REPORT_BUG'])) {
                        $name = $_POST['bug_name'];
                        $description = $_POST['bug_desc'];
                        $server = $_POST['bug_server'];

                        $db->select('tasks_category', '*', null, "name='Reported Bugs' AND server=".$server);
//                        var_dump($db->getResult());
                        $id = intval($db->getResult()[0]['id']);
                        echo 'ID='.$id;
                        if ($db->numRows() <= 0) {
                            $db->insert('tasks_category', array('id' => null, 'server' => $server, 'name' => 'Reported Bugs', 'date_end' => null));
                        }

                        $db->insert('reported_bugs', array('id' => null, 'reported_by' => $user->getUserID(), 'name' => $name, 'description' => $description,
                                'server' => $server, 'date_added' => date('Y-m-d')));

                        $db->insert('tasks', array('id'=>null, 'name' => $name,'description' => $description, 'date_finished' => null, 'date_end' => null,
                                'date_started'=> date('Y-m-d'), 'server'=> $server, 'category'=> $id));

                        echo 'CAT_ID= '.$id;

                        echo 'Bug reported successfully';
                    }else

                    if (isset($_POST['REMOVE_TASK'])){
                        $id = $_POST['REMOVE_TASK'];

                        $db->delete('tasks', 'id='.$id);
//                        $db->insert('tasks_removed', array('name' => ));
                    }else

                        if (isset($_POST['COMPLETE_TASK'])){
                            $id = $_POST['COMPLETE_TASK'];

                            $db->update('tasks', array('finished' => '1', 'date_finished' => date('Y-m-d')), 'id='.$id);
                        }
                    else

                        if (isset($_POST['UNCOMPLETE_TASK'])){
                            $id = $_POST['UNCOMPLETE_TASK'];

                            $db->update('tasks', array('finished' => 0), 'id='.$id);
                        }else

                        if (isset($_POST['EDIT_TASK'])){
                            $data = array('task_name' => $_POST['task_name'], 'task_desc' => $_POST['task_desc'], 'end_date' => $_POST['end_date']);

                            $_SESSION['edit_task_data'] = $data;
                        }
                    else
                        if (isset($_POST['ADD_TASK'])){
                            $name = $_POST['task_name'];
                            $desc = $_POST['task_desc'];
                            $category = $_POST['task_category'];
                            $server = $_POST['task_server'];
                            $date = $_POST['date_end'];

                            $db->insert('tasks', array('name' => $name, 'description' => $desc, 'date_end' => $date,
                                'date_started' => date('Y-m-d'), 'server' => $server, 'category' => $category));
                        }
                        else
                            if (isset($_POST['ADD_CATEGORY'])){
                                $name = $_POST['category_name'];
                                $server = $_POST['category_server'];

                                $db->insert('tasks_category', array('name' => $name, 'server' => $server));
                            }

        $db->disconnect();
    }
}