<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 19:17
 */

require_once '../dist/php/SessionUser.php';
require_once '../dist/php/Database.php';
require_once '../dist/php/Functions.php';
require_once '../dist/php/Role.php';
require_once '../dist/php/MinecraftUUID.php';

$user = new SessionUser();
$db = new Database();
$func = new Functions();
$role = new Role();
$uuidMC = new MinecraftUUID();

$can_log = false;
if ($user->isLoggedIn()) {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        var_dump($_POST);
        $db->connect();
        if (isset($_POST['ADD_SERVER'])) {
            $server = $_POST['ADD_SERVER'];
            $user_id = $_POST['USER_ID'];

            $db->update('user_data', array('server_owner' => $server), 'id='.$user_id);
            $_SESSION['user_data']['server_owner'] = $server;
        }
        $db->disconnect();
    }
}