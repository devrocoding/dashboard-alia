<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 19-1-2018
 * Time: 16:07
 */
?>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="semantic/dist/semantic.min.js"></script>

<script src="http://www.chartjs.org/samples/latest/utils.js"></script>

<script src="bower_components/fastclick/lib/fastclick.js"></script>

<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!--<script src="https://trello.com/1/client.js?key=59405ae07391b3df439d6daa"></script>-->

<script src="node_modules/izimodal/js/iziModal.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- PACE -->
<!--<script src="bower_components/PACE/pace.min.js"></script>-->

<!--<script src="dist/js/demo.js"></script>-->
<script src="dist/js/statitics.js"></script>
<script src="dist/js/global_stats.js"></script>

<script src="dist/js/script.js"></script>

<!-- ALL Pages -->
<script src="dist/js/user.js"></script>
<script src="dist/js/report_bug.js"></script>
<script src="dist/js/tasks.js"></script>
<script src="dist/js/profile.js"></script>

<!--<script type="text/javascript" src="./assets/js/jquery.2.0.0.js"></script>-->
<!---->
<!--<script src="https://use.fontawesome.com/a20e50059a.js"></script>-->
<!--<script src="./node_modules/sweetalert/dist/sweetalert.min.js"></script>-->
<!--<script src="./assets/js/rainbow.js"></script>-->
<!---->
<!--<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>-->
<!---->
<!---->
<!--<script  src="./pdf/js/jspdf.min.js"></script>-->
<!--<script  src="./pdf/js/pdf.js?v=jino_05_11_2017_12"></script>-->
<!--<script  src="./pdf/js/jspdf.plugin.autotable.js"></script>-->
<!--<script  src="./pdf/js/jsPdf_Plugins.js"></script>-->
<!--<script src="./pdf/js/pdf.js" type="text/javascript"></script>-->
