<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 20-1-2018
 * Time: 15:42
 */

$func = new Functions();

?>

<section class="content-header">
    <h1>
        <?php if (file_exists($func->getPageFromCurrentUrl())){ ?>
            <?php /** TODO: Show page name */ echo $func->getPageName(); ?>
        <?php }else{ ?>
            <?php echo 'Error'; ?>
        <?php } ?>
        <small>EnchantedMC Dashboard</small>
    </h1>
    <ol class="breadcrumb">
        <?php $crumbs = explode("/",$_SERVER["SCRIPT_FILENAME"]); ?>
        <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo 'Dashboard'; //Hardcoded! FUCK YEA ?></a></li>
        <?php if (file_exists($func->getPageFromCurrentUrl())){ ?>
            <li class="active"><?php echo ucfirst($func->getPageName()); ?></li>
        <?php }else{ ?>
            <li class="active"><?php echo '404'; ?></li>
        <?php } ?>
        <?php $user_name = $func->getUserNameFromCurrent(); ?>
        <?php if($user_name != null){ ?>
            <li class="active"><?php echo $user_name; ?></li>
        <?php } ?>
    </ol>
</section>
