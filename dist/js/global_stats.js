
var color = Chart.helpers.color;
var barChartData = {
    labels: [top_3_active[0]['name'], top_3_active[1]['name'], top_3_active[2]['name']],
    datasets: [{
        label: "First",
        backgroundColor: ["#ff8080", "#8e5ea2","#ffa64d"],
        borderColor: ["#ff8080", "#8e5ea2","#ffa64d"],
        borderWidth: 3,
        data: [
            top_3_active[0]['data'],
            top_3_active[1]['data'],
            top_3_active[2]['data']
        ]
    }]

};

var config = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                most_played_servers["prison"],
                most_played_servers["survival"],
                most_played_servers["skyblock"],
                most_played_servers["factions"]
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.green
            ],
            label: 'Dataset 1'
        }],
        labels: SERVERS
    },
    options: {
        responsive: true
    }
};

window.onload = function() {
    var pie = document.getElementById("pie_global");
    window.pie_global = new Chart(pie, config);

    var chart = document.getElementById("chart-global");
    window.chart_global = new Chart(chart, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: { display: false }
        }
    });
};