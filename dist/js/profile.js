$( document ).ready(function () {

    $( "#server_owner" ).change(function() {
        var val = $(this).val();
        var user_id = $(this).attr('user_id');

        $("#change_settings").addClass('loading');
        $.ajax({
            type: "POST",
            data: '&ADD_SERVER='+val+'&USER_ID='+user_id,
            url: "./base/profile_request.php",
            success: function (data) {
                $('#change_settings').removeClass('loading');
                // alert(data);
                window.location.reload();
            }
        });
    });

});