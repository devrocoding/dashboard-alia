$( document ).ready(function () {

    $('#report_bug')
        .form({
            fields: {
                bug_name: {
                    identifier: 'bug_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'This is empty'
                        }
                    ]
                },
                bug_server: {
                    identifier: 'bug_server',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'There is nothing select here'
                        }
                    ]
                }
            },inline : true,
            onSuccess: function(event, fields) {
                event.preventDefault();
                $(this).addClass('loading');
                $.ajax({
                    type: "POST",
                    data: $('#report_bug').serialize() + '&REPORT_BUG=true',
                    url: "./base/dashboard_request.php",
                    success:function(data) {
                        $('#report_bug').removeClass('loading');
                        alert(data);
                        $('#report_bug').form('clear');
                        var modal = $('#modal').iziModal();
                        modal.iziModal('setTitle', data);
                        modal.iziModal('setSubtitle', 'Response Code: SUCCES');
                        modal.iziModal('open',{});
                    }
                });
            }
        });

});