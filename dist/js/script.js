$( document ).ready(function () {

    $("#modal").iziModal();

    // var data_ids = ['add_user', 'all_users', 'statitics', 'trello', 'tasks', 'add_task'];
    //
    // for (i = 0;i<data_ids.length;i++){
    //     $('body').delegate('#'+data_ids[i]+'_side', 'click', function () {
    //         // alert('test');
    //         $('.active').removeClass('active');
    //         $(this).addClass('active');
    //     });
    // }

    // LOGOUT
    $('#sign_out').click(function () {
        var id = $(this).attr('user_id');
        $('.ui.basic.modal')
            .modal({
                closable  : false,
                onApprove : function() {
                    $.ajax({
                        type: "POST",
                        data: 'LOGOUT=SAVE',
                        url: "./base/dashboard_request.php",
                        success: function (data) {
                            // alert(data);
                            window.location.reload();
                        }
                    });
                }
            })
            .modal('show')
        ;
    });

});