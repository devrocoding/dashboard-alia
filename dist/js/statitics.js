var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

window.randomScalingFactor = function() {
    return Math.round(Samples.utils.rand(0, 100));
};

function isAvailable(number) {
    return number > 0;
}

// function getWeekNumber(d) {
//     d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
//     d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
//     var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
//     var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
//     return [d.getUTCFullYear(), weekNo];
// }
//
// function getWeekNumbersFormat() {
//     var how_many_weeks = 5;
//
//     var a = new Array(how_many_weeks - 1);
//     var count = 0;
//     var result = getWeekNumber(new Date());
//     // var result2 = result.split(',')
//
//     for (i = result[1] - how_many_weeks; i <= result[1]; i++) {
//         if (i <= 0)
//             continue;
//         a[count] = 'week ' + i + ' - ' + result[0];
//         count++;
//     }
//     return a;
// }

// var line_stats_config = {
//     type: 'bar',
//     data: {
//         labels: getWeekNumbersFormat(),
//         datasets: [{
//             label: "Prison",
//             fill: false,
//             borderColor: window.chartColors.yellow,
//             backgroundColor: window.chartColors.yellow,
//             data: [
//                 staff_online_data['prison'][1],
//                 staff_online_data['prison'][2],
//                 staff_online_data['prison'][3],
//                 staff_online_data['prison'][4],
//             ]
//         }, {
//             label: "Factions",
//             fill: false,
//             borderColor: window.chartColors.blue,
//             backgroundColor: window.chartColors.blue,
//             data: [
//                 staff_online_data['factions'][1],
//                 staff_online_data['factions'][2],
//                 staff_online_data['factions'][3],
//                 staff_online_data['factions'][4],
//             ]
//         }, {
//             label: "Survival",
//             fill: false,
//             borderColor: window.chartColors.red,
//             backgroundColor: window.chartColors.red,
//             data: [
//                 staff_online_data['survival'][1],
//                 staff_online_data['survival'][2],
//                 staff_online_data['survival'][3],
//                 staff_online_data['survival'][4],
//             ]
//         }, {
//             label: "Skyblock",
//             fill: false,
//             borderColor: window.chartColors.purple,
//             backgroundColor: window.chartColors.purple,
//             data: [
//                 staff_online_data['skyblock'][1],
//                 staff_online_data['skyblock'][2],
//                 staff_online_data['skyblock'][3],
//                 staff_online_data['skyblock'][4],
//             ]
//         }]
//     },
//     options: {
//         responsive: true,
//         title:{
//             display: true,
//             text:"Server Online Statitics | Task Counter"
//         },
//         scales: {
//             xAxes: [{
//                 display: true,
//             }],
//             yAxes: [{
//                 display: true,
//                 beginAtZero: false
//             }]
//         }
//     }
// };

var SERVERS = ["Prison", "Survival", "Skyblock", "Factions"];

////// PIE YOUR STATS DAILY

var pie_self_config_daily = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                staff_online_data['daily']['prison'],
                staff_online_data['daily']['survival'],
                staff_online_data['daily']['skyblock'],
                staff_online_data['daily']['factions']
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.purple,
            ],
            label: 'Dataset 1'
        }],
        labels: SERVERS
    },
    options: {
        responsive: true
    }
};

////// PIE WEEKLY CONFIG

var pie_self_config_weekly = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                staff_online_data['weekly']['prison'],
                staff_online_data['weekly']['survival'],
                staff_online_data['weekly']['skyblock'],
                staff_online_data['weekly']['factions']
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.purple,
            ],
            label: 'Dataset 1'
        }],
        labels: SERVERS
    },
    options: {
        responsive: true
    }
};

var pie_self_config_monthly = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                staff_online_data['monthly']['prison'],
                staff_online_data['monthly']['survival'],
                staff_online_data['monthly']['skyblock'],
                staff_online_data['monthly']['factions']
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.purple,
            ],
            label: 'Dataset 1'
        }],
        labels: SERVERS
    },
    options: {
        responsive: true
    }
};

var pie_self_config_yearly = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                staff_online_data['yearly']['prison'],
                staff_online_data['yearly']['survival'],
                staff_online_data['yearly']['skyblock'],
                staff_online_data['yearly']['factions']
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.purple,
            ],
            label: 'Dataset 1'
        }],
        labels: SERVERS
    },
    options: {
        responsive: true
    }
};

////// PIE CHART

var pie_global_config = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
            ],
            backgroundColor: [
                window.chartColors.red,
                window.chartColors.orange,
                window.chartColors.yellow,
                window.chartColors.green,
                window.chartColors.blue,
            ],
            label: 'Dataset 1'
        }],
        labels: SERVERS
    },
    options: {
        responsive: true
    }
};

//--------------
//- AREA CHART -
//--------------

var area_global_config = {
    type: 'line',
    data: {
        labels: ['Test'],
        datasets: [{
            label: "My First dataset",
            borderColor: window.chartColors.red,
            backgroundColor: window.chartColors.red,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: "My Second dataset",
            borderColor: window.chartColors.blue,
            backgroundColor: window.chartColors.blue,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: "My Third dataset",
            borderColor: window.chartColors.green,
            backgroundColor: window.chartColors.green,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }, {
            label: "My Third dataset",
            borderColor: window.chartColors.yellow,
            backgroundColor: window.chartColors.yellow,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ],
        }]
    },
    options: {
        responsive: true,
        title:{
            display:true,
            text:"Chart.js Line Chart - Stacked Area"
        },
        tooltips: {
            mode: 'index',
        },
        hover: {
            mode: 'index'
        },
        scales: {
            xAxes: [{
                scaleLabel: {
                    display: true,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                stacked: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Value'
                }
            }]
        }
    }
};

window.onload = function() {
    var ctx;
    if (document.getElementById("area_global") != null) {
        ctx = document.getElementById("area_global").getContext("2d");
        window.myLine = new Chart(ctx, area_global_config);
    }

    if (document.getElementById("line_stats") != null) {
        ctx = document.getElementById("line_stats").getContext("2d");
        window.myLine = new Chart(ctx, line_stats_config);
    }

    if (document.getElementById("pie_global") != null) {
        ctx = document.getElementById("pie_global").getContext("2d");
        window.myPie = new Chart(ctx, pie_global_config);
    }

    if (document.getElementById("pie_stats_daily") != null) {
        ctx = document.getElementById("pie_stats_daily").getContext("2d");
        window.myPie = new Chart(ctx, pie_self_config_daily);
    }

    if (document.getElementById("pie_stats_weekly") != null) {
        ctx = document.getElementById("pie_stats_weekly").getContext("2d");
        window.myPie = new Chart(ctx, pie_self_config_weekly);
    }

    if (document.getElementById("pie_stats_monthly") != null) {
        ctx = document.getElementById("pie_stats_monthly").getContext("2d");
        window.myPie = new Chart(ctx, pie_self_config_monthly);
    }

    if (document.getElementById("pie_stats_yearly") != null) {
        ctx = document.getElementById("pie_stats_yearly").getContext("2d");
        window.myPie = new Chart(ctx, pie_self_config_yearly);
    }
};