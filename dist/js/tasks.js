$( document ).ready(function () {

    $('#add_task').click(function () {
        $('.ui.add-task.modal')
            .modal('show');
    });

    $('#tasks_add_category').click(function () {
        $('.ui.add-category.modal')
            .modal('show');
    });

    $('#add_task_form')
        .form({
            fields: {
                task_name: {
                    identifier: 'task_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'This is empty'
                        }
                    ]
                },
                task_desc: {
                    identifier: 'task_desc',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'There is nothing select here'
                        }
                    ]
                },
                task_server: {
                    identifier: 'task_server',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'There is nothing select here'
                        }
                    ]
                },
                date_end: {
                    identifier: 'date_end',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'There is nothing select here'
                        }
                    ]
                }
            },inline : true,
            onSuccess: function(event, fields) {
                event.preventDefault();
                $(this).addClass('loading');
                $.ajax({
                    type: "POST",
                    data: $('#add_task_form').serialize() + '&ADD_TASK=true',
                    url: "./base/dashboard_request.php",
                    success:function(data) {
                        $('#add_task_form').removeClass('loading');
                        window.location.reload();
                    }
                });
            }
        });

    $('#add_category_form')
        .form({
            fields: {
                task_name: {
                    identifier: 'task_name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'This is empty'
                        }
                    ]
                },
                task_desc: {
                    identifier: 'task_desc',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'There is nothing select here'
                        }
                    ]
                },
                task_server: {
                    identifier: 'task_server',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'There is nothing select here'
                        }
                    ]
                },
                date_end: {
                    identifier: 'date_end',
                    rules: [
                        {
                            type: 'empty',
                            prompt: 'There is nothing select here'
                        }
                    ]
                }
            },inline : true,
            onSuccess: function(event, fields) {
                event.preventDefault();
                $(this).addClass('loading');
                $.ajax({
                    type: "POST",
                    data: $('#add_category_form').serialize() + '&ADD_CATEGORY=true',
                    url: "./base/dashboard_request.php",
                    success:function(data) {
                        $('#add_category_form').removeClass('loading');
                        // alert(data);
                        window.location.reload();
                    }
                });
            }
        });

    $('.remove_task').click(function (){
        // alert("test");
        var id = $(this).attr('task_id');

        $.ajax({
            type: "POST",
            data: '&REMOVE_TASK='+id,
            url: "./base/dashboard_request.php",
            success:function(data) {
                // alert(data);
                loadContent( '#reload_ajax_tasks', document.URL, '#reload_ajax_tasks' );
                window.location.reload();
            }
        });
    });

    // $('.edit_task').click(function (){
    //     var id = $(this).attr('task_id');
    //     var name = $(this).attr('task_name');
    //     var endDate = $(this).attr('end_date');
    //     var desc = $(this).attr('task_desc');
    //
    //     $.ajax({
    //         type: "POST",
    //         data: '&EDIT_TASK='+id+'&task_name='+name+'&task_desc='+desc+'end_date'+endDate,
    //         url: "./base/dashboard_request.php",
    //         success:function(data) {
    //             $('.ui.add-task.modal')
    //                 .modal({
    //                     closable  : false,
    //                     onDeny    : function(){
    //                         // window.location.reload();
    //                         return false;
    //                     }
    //                 })
    //                 .modal('show');
    //         }
    //     });
    // });

    $('.complete_task').click(function (){
        // alert("test");
        var id = $(this).attr('task_id');

        $.ajax({
            type: "POST",
            data: '&COMPLETE_TASK='+id,
            url: "./base/dashboard_request.php",
            success:function(data) {
                loadContent( '#reload_ajax_tasks', document.URL, '#reload_ajax_tasks' );
                window.location.reload();
            }
        });
    });

    $('.uncomplete_task').click(function (){
        // alert("test");
        var id = $(this).attr('task_id');

        $.ajax({
            type: "POST",
            data: '&UNCOMPLETE_TASK='+id,
            url: "./base/dashboard_request.php",
            success:function(data) {
                // alert(data);
                // $('#reload_ajax_tasks').load(document.URL +  ' #reload_ajax_tasks');
                loadContent( '#reload_ajax_tasks', document.URL, '#reload_ajax_tasks' );
                window.location.reload();
            }
        });
    });

    function loadContent(target, url, selector) {
        $.ajax({
            url: url,
            success: function(data,status,jqXHR) {
                $(target).html($(data).find(selector).addBack(selector).children())
                    .fadeIn('fast', hideLoader());
            }
        });
    }

});