$( document ).ready(function () {

    $('#add_user')
        .form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Email is not filled in'
                        },
                        {
                            type   : 'email',
                            prompt : 'Email is not correct'
                        }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Password is not filled in'
                        }
                    ]
                },
                minecraft: {
                    identifier: 'minecraft',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Minecraft username is not filled in'
                        }
                    ]
                },
                name: {
                    identifier: 'name',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Username is not filled in'
                        }
                    ]
                },
                role: {
                    identifier  : 'role',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select a role'
                        }
                    ]
                },
                image: {
                    identifier  : 'image',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please select a image'
                        }
                    ]
                }
            },inline : true,
            on     : 'blur',
            onSuccess: function(event, fields) {
                event.preventDefault();
                $(this).addClass('loading');
                $.ajax({
                    type: "POST",
                    data: $('#add_user').serialize() + '&ADD_USER=true',
                    url: "./base/dashboard_request.php",
                    success:function(data) {
                        $('#add_user').removeClass('loading');

                        $('#add_user').form('clear');
                        var modal = $('#modal').iziModal();
                        modal.iziModal('setTitle', data);
                        modal.iziModal('setSubtitle', 'Response Code: SUCCES');
                        modal.iziModal('open',{});
                    }
                });
            }
        });

    $('.remove_user').click(function () {
        var id = $(this).attr('user_id');
        $('.ui.basic.modal')
            .modal({
                closable  : false,
                onApprove : function() {
                    $.ajax({
                        type: "POST",
                        data: '&DELETE_USER='+id,
                        url: "./base/dashboard_request.php",
                        success: function (data) {
                            window.location.reload();
                        }
                    });
                }
            })
            .modal('show')
        ;
    });

});