<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 09:49
 */
require_once 'Profile.php';
require_once 'Tasks.php';

class functions
{

    public $short = 'q=';

    public function serialize($data)
    {
        return $this->base64_url_encode($data);
    }

    public function getHomeData()
    {
        return '?' . $this->short . $this->serialize('index');
    }

    public function getActiveState($page)
    {
        if (isset($_GET)){
            if (isset($_GET[str_replace('=', '', $this->short)])){
//                echo strtolower($page) . ' ' . strtolower($this->base64_url_decode($_GET[str_replace('=', '', $this->short)])) . ' ';
                if (strpos(strtolower($page), strtolower($this->base64_url_decode($_GET[str_replace('=', '', $this->short)]))) !== false){
                    return 'active';
                }
            }
        }
        return '';
    }

    public function gotoPage($page, $path = null, $user_name = null, $user_id = null, $task_id = null)
    {
        $path_url = '';

        $url = $this->base64_url_encode($page);
        $query = $_GET;
        $query[str_replace('=', '', $this->short)] = $url;
        if (is_array($path)) {
            foreach ($path as $p) {
                $path_url .= '/'.$p;
            }
            $query['p'] = $this->base64_url_encode($path_url);
        }else{
            unset($query['p']);
        }
        if ($user_name != null && $user_id != null){
            $seri = $user_name . '/' . $user_id;
            $query['user'] = $this->base64_url_encode($seri);
        }else{
            unset($query['user']);
        }
        if ($task_id != null){
            $query['t'] = $this->base64_url_encode($task_id);
        }else{
            unset($query['t']);
        }
        $query_result = http_build_query($query);
        return $_SERVER['PHP_SELF'] . '?' . $query_result;
    }

    public function getPageName()
    {
        $path = './';
        $page = $this->base64_url_decode($_GET[str_replace('=', '', $this->short)]);
        $org_page =  $path . '/' . $page . ".php";
//        $page = str_replace(str_split('.php.//'), '', $org_page);
        $f = str_replace('_', ' ', $page);
        $final = '';


        foreach (explode(' ', $f) as $str){
            if (isset($_GET['t'])) {
                $tasks = new Tasks();
                $id = $this->base64_url_decode($_GET['t']);
                $task = $tasks->getServerByID($id)[0]['name'];
                $str = str_replace('{name}', '('.$task.')', $str);
            }
            $final .= ucfirst($str) . ' ';
        }
        return $final;
    }

    public function getPageFromCurrentUrl(){
        if (isset($_GET[str_replace('=', '', $this->short)])) {
            $path = './';
            if (isset($_GET['p'])){
                $path = '.' . $this->base64_url_decode($_GET['p']);
            }
            $page = $this->base64_url_decode($_GET[str_replace('=', '', $this->short)]);
            return $path . '/' . $page . ".php";
        }
    }

    public function readPage($page){
        if ($this->getUserNameFromCurrent() != null) {
            $p = new Profile();
            if ($p->getUser() == null){
                http_response_code(400);
                $page = '400.php';
            }
        }
        if (!file_exists($page)) {
            http_response_code(404);
            $page = '404.php';
        }
        return include_once $page;
    }

    public function getUserNameFromCurrent()
    {
        if (isset($_GET['user'])){
            $deseri = $this->base64_url_decode($_GET['user']);
            $name = explode('/', $deseri)[0];
            return $name;
        }
        return null;
    }

    public function getRandomColor()
    {
        $rand = rand(1, 10);
        switch($rand){
            case 1: return 'primary'; break;
            case 2: return 'success'; break;
            case 3: return 'warning'; break;
            case 4: return 'danger'; break;
            case 5: return 'gray'; break;
            case 6: return 'navy'; break;
            case 7: return 'teal'; break;
            case 8: return 'purple'; break;
            case 9: return 'orange'; break;
            case 10: return 'maroon'; break;
            default: return 'black';
        }
    }

    public function getTaskServerID()
    {
        return $this->base64_url_decode($_GET['t']);
    }

    function base64_url_encode($input) {
        return strtr(base64_encode($input), '+/=', '._-');
    }

    function base64_url_decode($input) {
        return base64_decode(strtr($input, '._-', '+/='));
    }

    function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }

}