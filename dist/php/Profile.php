<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 25-1-2018
 * Time: 13:58
 */
require_once 'SessionUser.php';

class Profile extends SessionUser
{

    private $data = null;
    private $userClass;

    public function getUser()
    {
        if (isset($_GET)){
            if (isset($_GET['user'])){
                $user_name = $this->base64_url_decode($_GET['user']);
                $deseri = explode('/', $user_name);

                if (!isset($deseri[0]) || !isset($deseri[1])){
                    return null; //TODO: Fix user not found
                }
                if ($deseri[1] == $this->getUserID()){
                    return new SessionUser();
                }
                $this->connect();
                $this->data = $this->select('user_data', '*', null, 'id=' . $deseri['1']);
                if ($this->numRows() <= 0){
                    return null; //TODO: Fix user not found
                }
                $this->disconnect();
                $this->userClass = new User();
                $this->userClass->selectUser($deseri['1']);
                return $this->userClass;
            }
        }
        return null;
    }

    function base64_url_decode($input) {
        return base64_decode(strtr($input, '._-', '+/='));
    }

    public function isOwnProfile()
    {
        $user_name = $this->base64_url_decode($_GET['user']);
        $deseri = explode('/', $user_name);
        return ($deseri[1] == $this->getUserID());
    }
}