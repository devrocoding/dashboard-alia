<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 22-1-2018
 * Time: 22:53
 */
require_once './vendor/autoload.php'; //loads all composer packages, including Predis
require_once 'User.php';

class RedisManager
{

    private $host = '127.0.0.1';
    private $port = 6379;
    private $password = '';

    private $connection = null;

    public function connect()
    {
        try{
            $this->connection = new Predis\Client(array(
                'scheme'   => 'tcp',
                'host'     => $this->host,
                'port'     => $this->port,
                'database' => 0
            ));
            if ($this->password != ''){
                $this->connection->auth($this->password);
            }
            $this->connection->connect();
            return $this->connection->isConnected();
        } catch (Exception $e){
            return null;
        }
    }

    public function getAllServers($uuid, $time)
    {
        if ($this->connection != null){
            $servers = $this->connection->hgetall('times:'.$time.':'.$uuid);
            if ($servers != null) {
                unset($servers['name']);
                return $servers;
            }
        }
        return null;
    }

    public function getHighestAllTime($uuid)
    {
        if ($this->connection != null){
            if (!$this->exists($uuid)){
                return '<span style="color: red"><b>error.</b></span>';
            }
            $servers = $this->getAllServers($uuid, 'all-time');
            $max = 0;
            foreach ($servers as $val){
                $val_check = intval($val);
                if ($val_check > $max){
                    $max = $val_check;
                }
            }
            return $max;
        }
        return null;
    }

    /**
     * @return null
     */
    public function getConnection()
    {
        return $this->connection;
    }

    public function getTopPlayers($amount = 3){

        if ($this->connection != null){
            $user = new User();
            $users = $user->getAllUsers();

            // '0' => array("name" => "JusJus", "data" => "200")
            $top3 = array();

            foreach ($users as $all){
                $mc_name = $all['minecraft_name'];
                $uuid = $all['minecraft_uuid'];
                $uuid_servers = $this->getAllServers($uuid, 'all-time');
                unset($uuid_servers['name']);
                if (is_array($uuid_servers)) {
                    foreach ($uuid_servers as $key => $val) {
                        for ($i = 0;$i<3;$i++) {
                            if (in_array($mc_name[$i], $top3)) {
                                if ($val > $top3[$key]['data']) {
                                    $server[$i] = array('data' => $val, 'name' => $mc_name);
                                }
                            } else {
                                $top3[$i] = array('data' => $val, 'name' => $mc_name);
                            }
                        }
                    }
                }
            }
            return $top3;

        }
        return null;
    }

    public function getMostPlayedServers()
    {
        if ($this->connection != null){
            $user = new User();
            $users = $user->getAllUsers();

            $server = array();

            foreach ($users as $all){
                $uuid = $all['minecraft_uuid'];
                $uuid_servers = $this->getAllServers($uuid, 'all-time');
                unset($uuid_servers['name']);
                if (is_array($uuid_servers)) {
                    foreach ($uuid_servers as $key => $val) {
                        if (in_array($key, $server)) {
                            if ($val > $server[$key]) {
                                $server[$key] = $val;
                            }
                        } else {
                            $server[$key] = $val;
                        }
                    }
                }
            }
            return $server;

        }
        return null;
    }

    public function exists($uuid)
    {
        if ($this->connection != null){
            return $this->connection->exists('times:all-time:'.$uuid);
        }
        return null;
    }

    public function existsTime($uuid, $time)
    {
        if ($this->connection != null){
            return $this->connection->exists('times:'.$time.':'.$uuid);
        }
        return null;
    }

    public function isConnected()
    {
        return $this->connection->isConnected();
    }

    public function getValueByKey($uuid, $key)
    {
        if ($this->connection != null){
            $servers = $this->connection->hget($uuid, $key);
            if ($servers != null) {
                return $servers;
            }
        }
        return null;
    }

    public function disconnect()
    {
        if ($this->connection != null){
            if ($this->connection->isConnected()){
                $this->connection->disconnect();
            }
        }
    }

}