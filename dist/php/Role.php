<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 12:45
 */

class Role
{

    public function canControlOperators($role = 6)
    {
        return $role <= 2;
    }

    public function canControlStaff($role = 6)
    {
        return $this->canControlOperators($role) || $role == 5;
    }

    public function canAdd($current, $roleToCheck)
    {
        return $roleToCheck > $current;
    }

    public function isStaff($role = 6)
    {
        return $role >= 6;
    }

    public function canReportBugs($role = 6)
    {
        return $role != 4;
    }

    public function isOperator($role = 6)
    {
        return $role >= 2 && $role <= 4;
    }

    public function formatRoleID($role)
    {
        switch($role){
            case 'OWNER':
                return 1;
                break;
            case 'MAN':
                return 2;
                break;
            case 'DEV':
                return 3;
                break;
            case 'OP':
                return 4;
                break;
            case 'HA':
                return 5;
                break;
            case 'STAFF':
                return 6;
                break;
            default: return 6;
        }
    }

}