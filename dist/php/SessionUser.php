<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 12:45
 */
//Everything for the user saved inside the session
require_once 'Database.php';

class SessionUser extends Database
{

    public function startSession()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function signOut()
    {
        $this->startSession();
        unset($_SESSION['user_data']);
    }

    public function getAllData()
    {
        $this->startSession();
        return $_SESSION['user_data'];
    }

    public function isLoggedIn()
    {
        $this->startSession();
        return isset($_SESSION['user_data']['id']);
    }

    public function getUserID()
    {
        $this->startSession();
        return $_SESSION['user_data']['id'];
    }

    public function getRoleID()
    {
        // Returns role id
        $this->startSession();
        return $_SESSION['user_data']['role'];
    }

    public function getAssingedServers()
    {
        $this->startSession();
        return $_SESSION['user_data']['server_owner'];
    }

    public function getReportedBugs()
    {
        return $_SESSION['reported_bugs'];
    }

    public function getMinecraftUUID()
    {
        $this->startSession();
        return $_SESSION['user_data']['minecraft_uuid'];
    }

    public function getRole()
    {
        // Returns role id
        $this->startSession();
        return $_SESSION['user_data']['role_name'];
    }

    public function getImage()
    {
        // Returns role id
        $this->startSession();
        return $_SESSION['user_data']['image'];
    }

    public function getImagePath()
    {
        // Returns role id
        $this->startSession();
        return 'dist/img/profile/'.$_SESSION['user_data']['image'];
    }

    public function getPayment()
    {
        $this->startSession();
        return $_SESSION['user_data']['payment'];
    }

    public function hasPayment()
    {
        $this->startSession();
        return $_SESSION['user_data']['payment'] > 0;
    }

    public function getPoints()
    {
        $this->startSession();
        return $_SESSION['user_data']['points'];
    }

    public function getMinecraftName()
    {
        $this->startSession();
        return $_SESSION['user_data']['minecraft_name'];
    }

    public function getNiceName()
    {
        // Returns the name of the user or the email
        $this->startSession();
        return ($_SESSION['user_data']['name'] != null)?$this->getName():$this->getEmail();
    }

    public function getName()
    {
        // Returns the name of the user
        $this->startSession();
        return $_SESSION['user_data']['name'];
    }

    public function getEmail()
    {
        // Returns the email of the user
        $this->startSession();
        return $_SESSION['user_data']['email'];
    }

}