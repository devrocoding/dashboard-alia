<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 31-1-2018
 * Time: 11:17
 */
require_once 'Database.php';

class Tasks extends Database
{

    public function getServers($owned = false, $id = null)
    {
        $this->connect();

        if (!$owned){
            $this->select('tasks_server');
        }else{
            $this->select('user_data','*', null, 'id='.$id);
            $servers = $this->getResult()[0]['server_owner'];
            $results = explode(',', $servers);

            $query = '';
            $count = 0;
            foreach ($results as $server){
                $id = intval($server);
                if ($server == null || $id <= 0)
                    continue;
                if ($count>0)
                    $query .= ' OR ';
                $query .= 'id='.$server;
                $count++;
            }

            $this->select('tasks_server', '*', null, $query);
        }

        if ($this->numRows() <= 0)
            return array();
        $result = $this->getResult();
        $this->disconnect();
        return $result;
    }

    public function getServerByID($id)
    {
        if ($id != null){
            $this->connect();
            $this->select('tasks_server', '*', null, 'id='.$id);
            if ($this->numRows() <= 0)
                return null;
            $result = $this->getResult();
            $this->disconnect();
            return $result;
        }
        return null;
    }

    public function hasServerID($serialized, $idToCheck){
        if (is_array($serialized)){
            foreach ($serialized as $ser){
                if ($idToCheck == $ser){
                    return true;
                }
            }
        }
        return false;
    }

    public function getCategories($server_id = null, $no_bugs = false)
    {
        $this->connect();
        if ($server_id != null) {
            if ($no_bugs != false) {
                $this->select('tasks_category', '*', null, 'server=' . $server_id . " AND NOT name='Reported Bugs'");
            }else{
                $this->select('tasks_category', '*', null, 'server=' . $server_id);
            }
        }else{
            $this->select('tasks_category');
        }

        if ($this->numRows() <= 0)
            return array();
        $result = $this->getResult();
        $this->disconnect();
        return $result;
    }

    public function getTasksByCategory($category_id, $server, $finished = 0)
    {
        $this->connect();
        if ($category_id != null){
            $this->select('tasks', '*', null, 'category='.$category_id.' AND server='.$server.' AND finished='.$finished);
        }else {
            $this->select('tasks', '*', null, 'server='.$server.' AND finished='.$finished);
        }
        if ($this->numRows() <= 0)
            return array();
        $result = $this->getResult();
        $this->disconnect();
        return $result;
    }

    public function getReportedBugs($user_id)
    {
        $this->connect();

        $this->select('reported_bugs', '*', null, 'reported_by='.$user_id);
        $result = $this->getResult();
        $this->disconnect();
        return $result;
    }

}