<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 5-12-2017
 * Time: 12:45
 */
require_once 'Database.php';

class User extends Database
{

    private $cache = null;

    public function getAllUsers()
    {
        $this->connect();
        $this->select('user_data');
        $result = $this->getResult();
        $this->disconnect();
        return $result;
    }

    public function selectUser($id)
    {
        $this->connect();
        $this->select('user_data', '*', null, 'id=' . $id);
        $this->cache = $this->getResult()[0];
        $this->disconnect();
    }

    public function isLoggedIn()
    {
        return isset($this->cache['id']);
    }

    public function getUserID()
    {
        return $this->cache['id'];
    }

    public function getAssingedServers()
    {
        $this->cache['server_owner'];
    }

    public function getRoleID()
    {
        return $this->cache['role'];
    }

    public function getMinecraftUUID()
    {
        return $this->cache['minecraft_uuid'];
    }

    public function getRole()
    {
        $this->connect();
        $this->select('role', 'name', null, 'id=' . $this->getRoleID());
        $res = $this->getResult()[0];
        $this->disconnect();
        return $res['name'];
    }

    public function getImage()
    {
        return $this->cache['image'];
    }

    public function getImagePath()
    {
        // Returns role id
        return 'dist/img/profile/'.$this->cache['image'];
    }

    public function getPayment()
    {
        return $this->cache['payment'];
    }

    public function hasPayment()
    {
        return $this->cache['payment'] > 0;
    }

    public function getPoints()
    {
        return $this->cache['points'];
    }

    public function getMinecraftName()
    {
        return $this->cache['minecraft_name'];
    }

    public function getReportedBugs()
    {
        return $this->cache['reported_bugs'];
    }

    public function getNiceName()
    {
        // Returns the name of the user or the email
        return ($this->cache['name'] != null)?$this->getName():$this->getEmail();
    }

    public function getName()
    {
        // Returns the name of the user
        return $this->cache['name'];
    }

    public function getEmail()
    {
        // Returns the email of the user
        return $this->cache['email'];
    }

}