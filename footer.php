<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 19-1-2018
 * Time: 16:01
 */
?>

<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="http://devrocoding.com" target="_blank">Psssttt... Secret</a>
    </div>
    <!-- Default to the left -->
    <strong>Made with <i class="fa fa-heart" style="color: #FA5882"></i> for <a href="http://enchantedmc.net" target="_blank">EnchantedMC</a></strong>
</footer>
