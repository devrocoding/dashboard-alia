<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 19-1-2018
 * Time: 16:01
 */
require_once 'dist/php/User.php';
require_once 'dist/php/Functions.php';
require_once 'dist/php/SessionUser.php';
require_once 'dist/php/Database.php';
require_once 'dist/php/Role.php';
require_once 'dist/php/RedisManager.php';
require_once 'dist/php/MinecraftUUID.php';
require_once 'dist/php/Tasks.php';

$session_user = new SessionUser();
$func = new Functions();
$redis = new RedisManager();
$mc = new MinecraftUUID();
$role = new Role();

if (!$session_user->isLoggedIn()){
    header("Location:./login");
}

if (!isset($_GET['q'])) {
    header('Location:' . $func->gotoPage('profile', array('sidebar', 'pages'), $session_user->getNiceName(), $session_user->getUserID()));
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include_once './base/imports.php'; ?>
<!--    --><?php //include_once './base/scripts.php'; ?>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-purple sidebar-mini">

<div class="ui basic modal">
    <div class="ui icon header">
        <i class="warning big icon"></i>
        Are you sure u wanne do this m8?
    </div>
    <div class="actions">
        <div class="ui red basic cancel inverted button">
            <i class="remove icon"></i>
            No
        </div>
        <div class="ui green ok inverted button">
            <i class="checkmark icon"></i>
            Yes
        </div>
    </div>
</div>

<div class="wrapper">

  <!-- Main Header -->
  <?php include_once './header.php'; ?>

  <!-- Left side column. contains the logo and sidebar -->
    <?php include_once './sidebar/sidebar.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content BreadCrumb / Header (Page header) -->
      <?php include_once './breadcrumb.php'; ?>

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="row">
            <div class="col-md-12">
                <!-- Modal structure -->
                <div id="modal" data-iziModal-icon="icon-home"></div>
<!--                <h1>Idea's</h1>-->
<!--                <ul>-->
<!--                    <li>Als er belangrijke informatie is... weergeven op dashboard en ze moeten aanvinken of ze het hebben gelezen of niet.</li>-->
<!--                    <li>Hun activiteit laten zien, als er punten afgaan en wanneer er punten bijgaan laten zien.</li>-->
<!--                </ul>-->
<!--                --><?php $func->readPage($func->getPageFromCurrentUrl()); ?>

            </div>
        </div>
    </section>
  </div>


    <?php if ($role->isOperator($session_user->getUserID()) || $role->canControlOperators($session_user->getUserID())){?>
        <!--    Modals for better control-->
        <?php include_once './sidebar/pages/add_task.php'; ?>
        <?php include_once './sidebar/pages/add_category.php'; ?>
    <?php } ?>

  <!-- Main Footer -->
  <?php include_once './footer.php'; ?>

  <!-- Settings Sidebar -->
<!--  --><?php //include_once './sidebar/sidebar_control.php'; ?>

</div>

<!-- REQUIRED JS SCRIPTS -->
<?php include_once './base/scripts.php'; ?>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
<script>
    $('#my-todo-list').todoList({
        onCheck: function(checkbox) {
            // Do something when the checkbox is checked
        },
        onUnCheck: function(checkbox) {
            // Do something after the checkbox has been unchecked
        }
    })
    // Table Maker
    // $(function () {
    //     $('#example1').DataTable({
    //         'paging'      : true,
    //         'lengthChange': false,
    //         'searching'   : true,
    //         'ordering'    : true,
    //         'info'        : true,
    //         'autoWidth'   : true
    //     })
    // })

    // Dropdown Init
    $('.ui.dropdown')
        .dropdown()
    ;
</script>
</body>
</html>