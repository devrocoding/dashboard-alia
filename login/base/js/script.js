$( document ).ready(function () {

    $('#login-form')
        .form({
            fields: {
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Email is not filled in'
                        },
                        {
                            type   : 'email',
                            prompt : 'Email is not correct'
                        }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Password is not filled in'
                        }
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                $(this).addClass('loading');

                $('.login-form-noacc-error-message').hide();
                $('.login-form-nopass-error-message').hide();
                $.ajax({
                    type: "POST",
                    data: $('.form').serialize() + '&USER_LOGIN=true',
                    url: "./login_request.php",
                    success:function(data) {
                        $('#login-form').removeClass('loading');
                        // alert(data);
                        if (data.includes('VALID')){
                            location.reload();
                        }else {
                            if (data.includes('CHANGE_PASSWORD')){
                                // alert(1);
                                window.location.href = 'change_password.php';
                            }else if (data.includes('ACCOUNT')){
                                $('.login-form-noacc-error-message').show();
                            }else if (data.includes('PASSWORD')){
                                $('.login-form-nopass-error-message').show();
                            }
                        }
                    }
                });
            }
        });

    $('#change-password-form')
        .form({
            fields: {
                password_first: {
                    identifier: 'password_first',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'U must fill in an password'
                        }
                    ]
                },
                password_second: {
                    identifier: 'password_second',
                    rules: [
                        {
                            type   : 'match[password_first]',
                            prompt : 'Passwords are not the same'
                        }
                    ]
                }
            },
            onSuccess: function(event, fields) {
                event.preventDefault();
                $(this).addClass('loading');

                $.ajax({
                    type: "POST",
                    data: $('.form').serialize() + '&CHANGE_PASSWORD=true',
                    url: "./login_request.php",
                    success:function(data) {
                        $('#change-password-form').removeClass('loading');
                        // alert(data);
                        if (data.includes('VALID')){
                            $('#info-message-password-change').hide();

                            $('#succes_message_change_password').show();
                            $('#succes_message_change_password').removeClass("hidden");
                        }
                    }
                });
            }
        });

    //------------------ Extra's ----------------\\

    $('select.dropdown')
        .dropdown();

    //--------------------------------------------\\

});

function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
                f.apply(context, args);
            },
            delay || 550);
    };
}

function sendData(serialized) {
    $.ajax({
        type: "POST",
        data: serialized,
        url: "./base/webshop_request.php",
        success:function(data) {
            // alert(data);
            return data.includes('SUCCES');
        }
    });
}