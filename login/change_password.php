<?php
session_start();
require_once '../dist/php/SessionUser.php';
$user = new SessionUser();

if ($user->isLoggedIn() || (!isset($_SESSION['ACTIVATE_PASSWORD_CHANGE']))){
    var_dump($_SESSION);
    header("Location: index.php");
}

$email = $_SESSION['ACTIVATE_PASSWORD_CHANGE'];

unset($_SESSION['ACTIVATE_PASSWORD_CHANGE']);

require_once 'login_request.php';
?>
<html>
<head>
    <?php include("base/imports.php"); ?>
</head>
<body>

<div class="ui container">
    <!--    <div class="ui red icon message">-->
    <!--        <i class="warning sign icon"></i>-->
    <!--        <div class="content">-->
    <!--            <div class="header">-->
    <!--                Dashboard Coming Soon-->
    <!--            </div>-->
    <!--            <p>This dashboard is coming soon, more information will follow.</p>-->
    <!--        </div>-->
    <!--    </div>-->
    <div class="login-form">
        <div class="ui middle aligned center aligned grid">
            <div class="column">
                <h2 class="ui blue image header">
                    <img class="image" src="../dist/img/logo.png" align="'">
                    <br>
                </h2>
                <form class="ui large form" id="change-password-form">
                    <div class="ui piled segment">
                        <div class="ui success message transition hidden" id="succes_message_change_password">
<!--                            <i class="close icon"></i>-->
                            <div class="header">
                                You have successful set your password.
                            </div>
                            <p>You may now log-in with the username you have chosen</p>
                            <p>Reload the page and login</p>
                        </div>
<!--                        INFO MESSAGE-->
                        <div class="ui info message" id="info-message-password-change">
                            <i class="close icon"></i>
                            <div class="header">
                                Make your new password!
                            </div>
                            <ul class="list">
                                <p>Choose your new password, u can always change <br>
                                    it later on your profile</p>
                            </ul>
                        </div>
                        <div class="field disabled">
                            <div class="ui left icon input">
                                <i class="at icon"></i>
                                <input type="email" name="email" value="<?php echo $email; ?>">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password_first" placeholder="New Password">
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui left icon input">
                                <i class="lock icon"></i>
                                <input type="password" name="password_second" placeholder="Password Again">
                            </div>
                        </div>
                        <div class="ui fluid large blue submit button">Change Password</div>
                        <div class="ui error message"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php include_once 'base/scripts.php'; ?>
</div>

</body>
</html>