<?php
session_start();
require_once '../dist/php/SessionUser.php';
$user = new SessionUser();
unset($_SESSION['ACTIVATE_PASSWORD_CHANGE']);

if ($user->isLoggedIn()){
    header("Location:../index.php");
}

require_once 'login_request.php';
?>
<html>
<head>
    <?php include("base/imports.php"); ?>
</head>
<body>

<div class="ui container">
<!--    <div class="ui red icon message">-->
<!--        <i class="warning sign icon"></i>-->
<!--        <div class="content">-->
<!--            <div class="header">-->
<!--                Dashboard Coming Soon-->
<!--            </div>-->
<!--            <p>This dashboard is coming soon, more information will follow.</p>-->
<!--        </div>-->
<!--    </div>-->
<div class="login-form">
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui blue image header">
                <img class="image" src="../dist/img/logo.png" align="'" style="width:50% !important; height:20% !important;">
                <br>
            </h2>
            <form class="ui large form" id="login-form">
                <div class="ui piled segment">

                    <!-- NO ACCOUNT ERROR-->
                    <div class="ui negative message hidden login-form-noacc-error-message">
                        <div class="header">
                            We can't find an account with that email.
                        </div>
                        <p>If you don't have an account, ask your manager</p>
                    </div>
                   <!--------------->

                    <!-- PASSWORD INCORRECT ERROR-->
                    <div class="ui negative message hidden login-form-nopass-error-message">
                        <div class="header">
                            There is an problem with your password!
                        </div>
                        <p>If your forgot your password, ask your manager to reset your password</p>
                    </div>
                    <!--------------->

                    <div class="field">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="email" placeholder="E-mail address">
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="ui fluid large blue submit button">Login</div>
                    <div class="ui error message"></div>
                </div>
            </form>
        </div>
    </div>
</div>
    <?php include_once 'base/scripts.php'; ?>
</div>

</body>
</html>