<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 24-1-2018
 * Time: 16:12
 */

$db = new Database();
$role_manager = new Role();
$func = new Functions();
$task_manager = new Tasks();
$user = new SessionUser();

$user->startSession();
$db->connect();
$server_id_cat = $task_manager->getServerByID(isset($_SESSION['server_id_task']) ? $_SESSION['server_id_task'] : null);
//$categories = $task_manager->getCategories(isset($_SESSION['server_id_task']) ? $_SESSION['server_id_task'] : null, true);

?>

    <div class="ui add-category modal">
        <i class="close icon"></i>
        <div class="header">
            Add a new category
        </div>
        <div class="content">
            <form class="ui form" id="add_category_form">
                <!--        <h4 class="ui dividing header">Add a new user to this dashboard</h4>-->
                <div class="sixteen wide field">
                    <label>Name of the category</label>
                    <div class="field">
                        <input type="text" name="category_name" placeholder="The name of the category">
                    </div>
                </div>
                <div class="sixteen wide field disabled">
                    <?php if (sizeof($server_id_cat) > 0){ ?>
                        <label>Wich server?</label>
                        <select class="ui fluid dropdown disabled" name="category_server" style="z-index: 9999 !important;">
                            <?php echo '<option value="">Select Server...</option>'; ?>
                            <?php echo '<option value="' . $server_id_cat[0]["id"] . '" selected>' . $server_id_cat[0]["name"] . '</option>'; ?>
                        </select>
                    <?php } ?>
                </div>
                <div class="ui submit button fluid teal">Add new category</div>
                <div class="ui error message"></div>
            </form>
        </div>
    </div>

<?php $db->disconnect(); ?>