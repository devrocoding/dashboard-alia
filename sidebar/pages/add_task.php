<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 24-1-2018
 * Time: 16:12
 */

$db = new Database();
$role_manager = new Role();
$func = new Functions();
$task_manager = new Tasks();
$user = new SessionUser();

$user->startSession();

$db->connect();
$categories = $task_manager->getCategories(isset($_SESSION['server_id_task']) ? $_SESSION['server_id_task'] : null, true);

$server_id_tasks = $task_manager->getServerByID(isset($_SESSION['server_id_task']) ? $_SESSION['server_id_task'] : null);
$numRows = $db->numRows();

$data = array('task_name' => null, 'task_desc' => null, 'end_date' => null);
if (isset($_SESSION['edit_task_data'])){
    $data = $_SESSION['edit_task_data'];
}

?>

<div class="ui add-task modal">
    <i class="close icon"></i>
    <div class="header">
        Add a new task
    </div>
    <div class="content">
        <form class="ui form" id="add_task_form">
            <!--        <h4 class="ui dividing header">Add a new user to this dashboard</h4>-->
            <div class="sixteen wide field">
                <label>Name of the task</label>
                <div class="field">
                    <input type="text" name="task_name" placeholder="The name of the task" value="<?php echo $data['task_name']; ?>">
                </div>
            </div>
            <div class="sixteen wide field">
                <label>Description</label>
                <textarea maxlength="70" rows="2" placeholder="Description for the operator or developer?" name="task_desc" value="<?php echo $data['task_desc']; ?>"></textarea>
            </div>
            <div class="sixteen wide field">
                <label>Wich category?</label>
                <select class="ui fluid dropdown" name="task_category">
                    <?php foreach ($categories as $tas) { ?>
                        <?php echo '<option value="">Select Category...</option>'; ?>
                        <?php echo '<option value="' . $tas["id"] . '">' . $tas["name"] . '</option>'; ?>
                    <?php } ?>
                </select>
            </div>
            <div class="fields">
                <div class="eight wide field disabled">
                    <?php if ($server_id_tasks > 0){ ?>
                        <label>Wich server?</label>
                        <select class="ui fluid dropdown disabled" name="task_server">
                            <?php echo '<option value="">Select Server...</option>'; ?>
                            <?php echo '<option value="' . $server_id_tasks[0]["id"] . '" selected>' . $server_id_tasks[0]["name"] . '</option>'; ?>
                        </select>
                    <?php } ?>
                </div>
                <div class="eight wide field">
                    <label>Deadline (Empty for no deadline)</label>
                    <div class="ui calendar" name="date_end">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="date" placeholder="Date/Time" name="date_end" value="<?php echo $data['end_date']; ?>">
                        </div>
                    </div>
                </div>
            </div>
            <?php if (sizeof($categories) > 0){ ?>
                <div class="ui submit button fluid teal">Add new task</div>
            <?php }else{ ?>
                <div class="ui negative message">
                    <div class="header">
                        No categories found in our task system
                    </div>
                    <p>You need to add a category
                    </p>
                </div>
            <?php } ?>
            <div class="ui error message"></div>
        </form>
    </div>
    <div class="actions">
        <div class="ui red deny button">
            Cancel
        </div>
    </div>
</div>

<?php $db->disconnect(); ?>