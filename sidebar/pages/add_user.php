<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 24-1-2018
 * Time: 01:38
 */

$db = new Database();
$role_manager = new Role();
$user = new SessionUser();

$db->connect();
$db->select('role');
$roles = $db->getResult();

?>
<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Information</h4>
    We are getting the minecraft UUID from mojang automatically. <br>
    They can choose their password on the first login. <br>
    Also we only support not every email server. If you need more, ask Justin
</div>
<div class="ui piled segment padded">
    <form class="ui form" id="add_user">
<!--        <h4 class="ui dividing header">Add a new user to this dashboard</h4>-->
        <div class="sixteen wide field">
            <label>Name (Displayed on the sidebar)</label>
            <div class="field">
                <input type="text" name="name" placeholder="Username">
            </div>
        </div>
        <div class="sixteen wide field">
            <label>Email</label>
            <div class="ui input">
                <input type="text" placeholder="example@gmail.com" name="email">
            </div>
        </div>
        <div class="sixteen wide field">
            <label>Profile Image</label>

            <select class="ui fluid dropdown" name="image">
                <option value="">Select Image...</option>
                    <?php $dirname = "dist/img/profile";
                    $images = glob($dirname."/*.{jpg,png,gif}", GLOB_BRACE);
                    foreach($images as $image) {
//                        echo '<div class="item">';
                        echo '<option value="'.basename($image).'"><img class="ui avatar image" src="dist/img/profile/' . basename($image) . '">'.str_replace(".{jpg,png,gif}", '', basename($image)).'</option>';
//                        echo '</div>';
                    }
                    ?>
            </select>

<!--            <div class="ui fluid search selection dropdown">-->
<!--                <input type="hidden" name="image">-->
<!--                <i class="dropdown icon"></i>-->
<!--                <div class="default text">Select Image</div>-->
<!--                <div class="menu">-->
<!--              --><?php //$dirname = "dist/img/profile";
//                    $images = glob($dirname."/*.{jpg,png,gif}", GLOB_BRACE);
//                    foreach($images as $image) {
//                        echo '<div class="item">';
//                        echo '<option value="'.basename($image).'"><img class="ui avatar image" src="dist/img/profile/' . basename($image) . '">'.str_replace(".{jpg,png,gif}", '', basename($image)).'</option>';
//                        echo '</div>'; }
//                ?>
<!--                </div>-->
<!--            </div>-->
        </div>
        <div class="sixteen wide field">
            <label>Minecraft Name (NOT UUID)</label>
            <div class="field">
                <input type="text" name="minecraft" placeholder="Minecraft Username">
            </div>
        </div>
        <div class="sixteen wide field">
            <label>Wich Rank?</label>
            <select class="ui fluid dropdown" name="role">
                <option value="">Select Role...</option>
                <?php foreach ($roles as $role){ ?>
                    <option value="<?php echo $role['short_name']; ?>" <?php echo $role_manager->canAdd($user->getRoleID(), $role['id']) ? '' : 'disabled'; ?>><?php echo $role['name']; ?></option>
                <?php } ?>
            </select>
        </div>
<!--        <div class="sixteen wide field">-->
<!--            <label>Monthly payment (Empty for no payment)</label>-->
<!--            <div class="ui right labeled input">-->
<!--                <label for="amount" class="ui label">$</label>-->
<!--                <input type="text" placeholder="Payment" id="amount" name="payment">-->
<!--                <div class="ui basic label">.00</div>-->
<!--            </div>-->
<!--        </div>-->

        <div class="ui submit button fluid teal">Add new user</div>
        <div class="ui error message"></div>
    </form>
</div>