<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 23-1-2018
 * Time: 14:07
 */

$redis = new RedisManager();
$role = new Role();
$p = new Profile();
$user = new User();
$users = $user->getAllUsers();

$useragent=$_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){ ?>
    <div class="alert alert-danger">
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        Unfortunately this function does not work on a small screen. <br>
        Only in a desktop view, or u can just use your computer lazy boi.
    </div>
<?php }else{ ?>
    <?php if(!$redis->connect()) { ?>
        <div class="alert alert-danger">
            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
            We can't connect to the database. So this system won't work without an database system.<br>
            Go to your manager if this error appears!
        </div>
        <?php return; ?>
    <?php } ?>

    <?php if ($redis->isConnected()){ ?>
            <div class="col-md-5">
                <div class="info-box">
                    <h3 class="box-title box-header">Most played servers</h3>
                    <div class="box-content">
                        <canvas class="" id="pie_global" height="110px" style="max-height: 450px; padding-bottom: : 1%;"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="info-box">
                    <h3 class="box-title box-header">Top 3 Active Users</h3>
                    <div class="box-content">
                        <canvas id="chart-global" style="max-height: 450px;"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All user online time</h3>
                        <div class="box-tools">
<!--                                <div class="input-group input-group-sm" style="width: 150px;">-->
<!--                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">-->
<!---->
<!--                                    <div class="input-group-btn">-->
<!--                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>-->
<!--                                    </div>-->
<!--                                </div>-->
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th style="width: 50px"></th>
                                <th style="width: 14em">User</th>
                                <th style="width: 14em">Overall Online Time</th>
                                <th>How many percent in a day</th>
                                <th style="width: 40px"></th>
                            </tr>
                            <?php foreach ($users as $user_for){ ?>
                                <?php $user_time = $redis->getHighestAllTime($user_for['minecraft_uuid']); ?>
                                <tr>
                                    <td><?php echo $user_for['name']; ?></td>
                                    <td><?php echo $user_for['minecraft_name']; ?></td>
                                    <td><?php echo $user_time; ?></td>
                                    <td>
                                        <div class="progress progress-xs">
                                            <div class="progress-bar progress-bar-danger" style="width: 100%"></div>
                                        </div>
                                    </td>
                                    <td><span class="badge bg-red">100%</span></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <script>
                <?php

                $servers = $redis->getMostPlayedServers();
                        $top3 = $redis->getTopPlayers(3);

                echo 'var most_played_servers' . ' = ' . json_encode($servers) . '; ';
                        echo 'var top_3_active' . ' = ' . json_encode($top3) . '; ';

                ?>
            </script>

            <?php $redis->disconnect(); ?>
        <?php } ?>
    <?php } ?>
<script src="bower_components/Chart.js/Chart.js"></script>
<script src="http://www.chartjs.org/dist/2.7.1/Chart.bundle.js"></script>