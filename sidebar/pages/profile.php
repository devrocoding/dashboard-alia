<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 25-1-2018
 * Time: 12:11
 */
require_once 'dist/php/Profile.php';

$p = new Profile();
$role = new Role();
$task = new Tasks();
$func = new Functions();
$session_user = new SessionUser();
$user = $p->getUser();

if ($user != null){

    $reported_bugs = $task->getReportedBugs($user->getUserID());
    $assinged_servers = $user->getAssingedServers();

?>

<div class="box box-widget widget-user">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-aqua-active" style="background: url('dist/img/banner.png') -120px 300px;">
        <h1 class="widget-user-username" style="font-weight: bolder; font-size: 40px"><?php echo $user->getNiceName(); ?></h1>
        <h4 class="widget-user-desc"><?php echo $user->getRole(); ?></h4>
    </div>
    <div class="widget-user-image">
        <?php if ($user->getImage() == null){ ?>
            <img src="dist/img/profile/no_user.png" class="img-circle" alt="User Image">
        <?php }else{ ?>
            <img src="<?php echo $user->getImagePath(); ?>" class="img-circle" alt="User Image">
        <?php } ?>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-sm-4 border-right">
                <div class="description-block">
                    <h5 class="description-header">N/A</h5>
                    <span class="description-text">Tasks Completed</span>
                </div>
                <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-4 border-right">
                <div class="description-block">
                    <h5 class="description-header">5333</h5>
                    <span class="description-text">Total Server Hours</span>
                </div>
                <!-- /.description-block -->
            </div>
            <!-- /.col -->
            <div class="col-sm-4">
                <div class="description-block">
                    <h5 class="description-header"><?php echo sizeof($reported_bugs); ?></h5>
                    <span class="description-text">Reported Bugs</span>
                </div>
                <!-- /.description-block -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
</div>
<div class="ui three column grid">
    <div class="column">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?php echo $user->getPoints(); ?></h3>
                <p>User Points</p>
            </div>
            <div class="icon" style="margin-top: 3%">
                <i class="ion ion-stats-bars"></i>
            </div>
<!--            <a href="#" class="small-box-footer">-->
<!--                See this activity <i class="fa fa-arrow-circle-right"></i>-->
<!--            </a>-->
        </div>
    </div>
    <?php if ($user->hasPayment()){ ?>
    <div class="column">
        <div class="small-box bg-green">
            <div class="inner">
                <h3>$<?php echo $user->getPayment(); ?> /m</h3>
                <p>Payment</p>
            </div>
            <div class="icon" style="margin-top: 3%">
                <i class="fa fa-dollar"></i>
            </div>
<!--            <a href="#" class="small-box-footer">-->
<!--                See Payment Activity <i class="fa fa-arrow-circle-right"></i>-->
<!--            </a>-->
        </div>
    </div>
    <?php }else{ ?>
        <div class="column">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo sizeof($reported_bugs); ?></h3>
                    <p>Reported Bugs</p>
                </div>
                <div class="icon" style="margin-top: 3%">
                    <i class="fa fa-dollar"></i>
                </div>
<!--                <a href="#" class="small-box-footer">-->
<!--                    See all reported bugs <i class="fa fa-arrow-circle-right"></i>-->
<!--                </a>-->
            </div>
        </div>
    <?php } ?>
    <div class="column">
        <div class="small-box bg-red">
            <div class="inner">
                <h3>N/A<sup style="font-size: 20px"></sup></h3>
                <p>Active on task system</p>
            </div>
            <div class="icon" style="margin-top: 3%">
                <i class="ion ion-pie-graph"></i>
            </div>
<!--            <a href="#" class="small-box-footer">-->
<!--                See Statitics <i class="fa fa-arrow-circle-right"></i>-->
<!--            </a>-->
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-8">
        <div class="nav-tabs-custom blue">
            <ul class="nav nav-tabs">
<!--                <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>-->
                <li class="active"><a href="#settings" data-toggle="tab">Change Settings</a></li>
                <li><a href="#password" data-toggle="tab">Change Password</a></li>
                <?php if ($role->canControlOperators($session_user->getRoleID()) /*&& !$p->isOwnProfile() */){ ?>
                    <li><a href="#owner" data-toggle="tab"><p style="font-weight: bold;">Owner Options</p></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
<!--                <div class="active tab-pane" id="activity">-->
                    <!-- Post -->
    <!--                <div class="post">-->
    <!--                    <div class="user-block">-->
    <!--                        <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">-->
    <!--                        <span class="username">-->
    <!--                          <a href="#">Jonathan Burke Jr.</a>-->
    <!--                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>-->
    <!--                        </span>-->
    <!--                        <span class="description">Shared publicly - 7:30 PM today</span>-->
    <!--                    </div>-->
    <!--                    <!-- /.user-block -->
    <!--                    <p>-->
    <!--                        Lorem ipsum represents a long-held tradition for designers,-->
    <!--                        typographers and the like. Some people hate it and argue for-->
    <!--                        its demise, but others ignore the hate as they create awesome-->
    <!--                        tools to help create filler text for everyone from bacon lovers-->
    <!--                        to Charlie Sheen fans.-->
    <!--                    </p>-->
    <!--                    <ul class="list-inline">-->
    <!--                        <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>-->
    <!--                        <li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a>-->
    <!--                        </li>-->
    <!--                        <li class="pull-right">-->
    <!--                            <a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments-->
    <!--                                (5)</a></li>-->
    <!--                    </ul>-->
    <!---->
    <!--                    <input class="form-control input-sm" type="text" placeholder="Type a comment">-->
    <!--                </div>-->
                    <!-- /.post -->
<!--                </div>-->
                <!-- /.tab-pane -->

                <div class="active tab-pane" id="settings">
                    <form class="ui form" id="change_user">
                        <div class="sixteen wide field disabled">
                            <label>Name (Displayed on the sidebar)</label>
                            <div class="field">
                                <input type="text" name="change_user[name]" placeholder="Username" value="<?php echo $user->getName(); ?>">
                            </div>
                        </div>
                        <div class="sixteen wide field disabled">
                            <label>Email</label>
                            <div class="ui input">
                                <input type="text" placeholder="example@gmail.com" name="change_user[email]" value="<?php echo $user->getEmail(); ?>">
                            </div>
                        </div>
                        <div class="sixteen wide field disabled">
                            <label>Profile Image</label>
                            <div class="ui fluid search selection dropdown">
                                <input type="hidden" name="change_user[image]">
                                <i class="dropdown icon"></i>
                                <div class="default text">Select Image</div>
                                <div class="menu">
                                    <?php $dirname = "dist/img/profile";
                                    $images = glob($dirname."/*.{jpg,png,gif}", GLOB_BRACE);
                                    $dataImg = $user->getImage();
                                    foreach($images as $image) {
                                        echo '<div class="item">';
                                        echo '<option value="'.basename($image).'">'.
                                                '<img class="ui avatar image" src="dist/img/profile/' . basename($image) . '">'.str_replace(".{jpg,png,gif}", '', basename($image)).'</option>';
                                        echo '</div>'; } ?>
                                </div>
                            </div>
                        </div>
                        <div class="sixteen wide field disabled">
                            <label>Minecraft Name (NOT UUID)</label>
                            <div class="field">
                                <input type="text" name="change_user[minecraft]" placeholder="Minecraft Username" value="<?php echo $user->getMinecraftName(); ?>">
                            </div>
                        </div>
                        <div class="ui submit button fluid teal disabled">Change Your Account</div>
                        <div class="ui error message"></div>
                    </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="password">
                    <form class="ui form" id="change_user">
                        <div class="sixteen wide field">
                            <label>New password</label>
                            <div class="field">
                                <input type="password" name="change_user[name]" placeholder="New password">
                            </div>
                        </div>
                        <div class="sixteen wide field">
                            <label>New password again</label>
                            <div class="ui input">
                                <input type="password" placeholder="New password again" name="password_check">
                            </div>
                        </div>
                        <div class="ui submit button fluid teal">Change Your Account</div>
                        <div class="ui error message"></div>
                    </form>
                </div>
                <div class="tab-pane" id="owner">
<!--                    <div class="ui buttons fluid">-->
<!--                        <div class="ui button">Give more payment</div>-->
<!--                        <div class="or"></div>-->
<!--                        <div class="ui positive button">Take some of their payment</div>-->
<!--                    </div>-->
                    <form class="ui form" id="change_settings">
                        <div class="sixteen wide field">
                            <label>Add servers to this user</label>
                            <select class="ui fluid search dropdown" multiple="" id="server_owner" user_id="<?php echo $user->getUserID(); ?>">
                                <option value="">Select server</option>
                                <?php $servers_deseri = explode(',', $assinged_servers); ?>
                                <?php $servers = $task->getServers(); ?>
                                <?php foreach ($servers as $server){ ?>
                                    <option value="<?php echo $server['id'].','; ?>" <?php echo $task->hasServerID($servers_deseri, $server['id']) ? 'selected' : ''; ?>><?php echo $server['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                                <!--        <h4 class="ui dividing header">Add a new user to this dashboard</h4>-->
                        <div class="sixteen wide field disabled">
                            <label>Payment per Month</label>
                            <div class="field">
                                <div class="ui right labeled input">
                                    <label for="amount" class="ui label">$</label>
                                    <input type="text" value="<?php echo $user->getPayment(); ?>" id="amount" class="disabled">
                                </div>
                            </div>
                            </div>
<!--                        <div class="ui submit button fluid teal">Change settings</div>-->
<!--                        <div class="ui error message"></div>-->
                    </form>
                </div>
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->

    <div class="col-md-4">
        <div class="ui segment">
            <h3 class="box-title ui center aligned">Last Reported Bugs</h3>
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <?php if (sizeof($reported_bugs) <= 0){ ?>
                        <div class="alert">
                            U didn't reported any bugs or problem yet!
                        </div>
                    <?php }else{ ?>
                        <?php for ($i=sizeof($reported_bugs)-1; $i>sizeof($reported_bugs)-5; $i--){?>
                            <?php $bug = $reported_bugs[$i]; ?>
                            <li class="item">
                                <div class="product-img">
                                    <img src="dist/img/ikonka_002-512.png" alt="Product Image">
                                </div>
                                <div class="product-info">
                                    <div class="product-title"><?php echo $bug['name']; ?>
                                        <span class="label label-warning pull-right"><?php echo $task->getServerByID($bug['server'])[0]['name']; ?></span></div>
                                    <span class="product-description">
                                      <?php echo $bug['description']; ?>
                                    </span>
                                </div>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
            <?php if (sizeof($reported_bugs) <= 0){ ?>
                <div class="box-footer text-center">
                    <a href="javascript:void(0)" class="uppercase">View All</a href="javascript:void(0)">
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php } ?>