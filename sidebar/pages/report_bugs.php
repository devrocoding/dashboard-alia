<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 24-1-2018
 * Time: 01:38
 */

$db = new Database();
$role_manager = new Role();
$user = new SessionUser();

$db->connect();
$db->select('tasks_server', '*', null, 'can_be_reported=1');
$tasks = $db->getResult();
$numRows = $db->numRows();

?>
<div class="alert alert-info alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Information</h4>
    Each bug comes automatically on the operator's tasks system. <br>
    Select the server / Type to add the bug too
</div>
<div class="ui piled segment padded">
    <form class="ui form" id="report_bug">
        <!--        <h4 class="ui dividing header">Add a new user to this dashboard</h4>-->
        <div class="sixteen wide field">
            <label>What's the problem?</label>
            <div class="field">
                <input type="text" name="bug_name" placeholder="The name of the bug">
            </div>
        </div>
        <div class="sixteen wide field">
            <label>Any information for the operator (Optional)</label>
            <textarea maxlength="70" rows="2" placeholder="Description for the operator or developer?" name="bug_desc"></textarea>
        </div>
        <div class="sixteen wide field">
            <?php if ($numRows > 0){ ?>
                <label>Wich server?</label>
                <select class="ui fluid dropdown" name="bug_server">
                    <?php foreach ($tasks as $tas) { ?>
                            <?php echo '<option value="">Select Server...</option>'; ?>
                            <?php echo '<option value="' . $tas["id"] . '">' . $tas["name"] . '</option>'; ?>
                    <?php } ?>
                </select>
            <?php } ?>
        </div>

        <?php if ($numRows > 0){ ?>
            <div class="ui submit button fluid yellow">Report bug</div>
            <div class="ui error message"></div>
        </form>
        <?php }else{ ?>
            </form>
            <div class="ui negative message">
                <div class="header">
                    No servers found in our task system
                </div>
                <p>Ask Justin to add an server if this error appears!
                </p></div>
        <?php } ?>
</div>

<?php $db->disconnect(); ?>