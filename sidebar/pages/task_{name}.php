<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 31-1-2018
 * Time: 10:54
 */
$func = new Functions();
$categories = new Tasks();

$server_id = $func->getTaskServerID();
$_SESSION['server_id_task'] = $server_id;

$cat = $categories->getCategories($server_id);

?>

<div class="row">
<!--    <div class="col-md-4">-->
<!--        <a class="btn btn-app">-->
<!--            <i class="fa fa-check"></i> See completed tasks-->
<!--        </a>-->
<!--    </div>-->
    <div class="pull-right" style="margin-right: 1%">
        <a class="btn btn-app" id="tasks_add_category">
            <i class="fa fa-plus-circle"></i> Add Category
        </a>
        <a class="btn btn-app" id="add_task">
            <i class="fa fa-plus"></i> Add task
        </a>
    </div>
</div>
<br>
<?php if (sizeof($cat) <= 0){ ?>
    <div class="alert alert-danger">
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        There are no categories to display for this server / main task
    </div>
<?php } ?>
<div id="reload_ajax_tasks">
    <div id="reload_ajax_tasks">
    <?php foreach ($cat as $c){ ?>
        <?php $tasks = $categories->getTasksByCategory($c['id'], $server_id); ?>
        <?php $tasksCompleted = $categories->getTasksByCategory($c['id'], $server_id, 1); ?>

<!--        --><?php //$completed_percent = round(100 / (sizeof($tasksCompleted)+1)); ?>
        <?php $uncompleted_percent = round(100 / (sizeof($tasks)+1)); ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><b><?php echo $c['name']; ?></b></h3>
                <div class="box-tools">
                    <span class="pull-right badge bg-red"><?php echo $uncompleted_percent; ?>%</span>
                    <div class="input-group input-group-sm" style="width: 75rem;">
                        <div class="progress progress" style="margin-right: 10px">
                            <div class="progress-bar" style="width: <?php echo $uncompleted_percent; ?>%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <?php if (sizeof($tasks) <= 0 && sizeof($tasksCompleted) <= 0){ ?>
                        <tr>
                            <td></td>
                            <td>No tasks to display for this category.</td>
                            <td></td>
                        </tr>
                    <?php }else{ ?>
                        <?php $i = 1; ?>
                        <?php foreach ($tasks as $task){ ?>
                            <?php $date = strtotime($task['date_end']); ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><h4><?php echo $task['name']; ?></h4></td>
                            <td><?php echo $task['description']; ?></td>
                            <td class=""><?php echo $task['date_end'] == 0 ? '' : '<div class="ui blue basic label">'.date( 'M d, Y', $date ).'</div>'; ?></td>
                            <td class="pull-right">
                                <a class="ui button remove_task" task_id="<?php echo $task['id']; ?>"><i class="fa fa-times"></i> Remove</a>
<!--                                <div class="ui teal buttons" style="position: absolute; right: 150px">-->
<!--                                    <a task_id="--><?php //echo $task['id']; ?><!--" task_name="--><?php //echo $task['name']; ?><!--" task_desc="--><?php //echo $task['description']; ?><!--" end_date="--><?php //echo $task['date_end']; ?><!--"-->
<!--                                       class="ui button edit_task">Edit</a>-->
<!--                                    <div class="ui floating dropdown icon button">-->
<!--                                        <i class="dropdown icon"></i>-->
<!--                                        <div class="menu">-->
<!--                                            <a class="item remove_task" task_id="--><?php //echo $task['id']; ?><!--"><i class="fa fa-times"></i> Remove</a>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <a task_id="<?php echo $task['id']; ?>" class="ui button positive complete_task"><i class="thumbs outline up icon"></i> Complete</a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                        <?php } ?>

                        <?php foreach ($tasksCompleted as $task){ ?>
                            <tr>
                                <td></td>
                                <td><STRIKE><?php echo $task['name']; ?></STRIKE></td>
                                <td><?php echo $task['description']; ?></td>
                                <td></td>
                                <td class="pull-right">
                                    <div class="ui red buttons">
                                        <a task_id="<?php echo $task['id']; ?>" class="ui button uncomplete_task">Mark Unfinished``</a>
                                        <div class="ui floating dropdown icon button"style="position: absolute; right: 0px; margin-right: 10px">
                                            <i class="dropdown icon"></i>
                                            <div class="menu">
                                                <a class="item remove_task" task_id="<?php echo $task['id']; ?>"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    <?php } ?>

    </div>
</div>
