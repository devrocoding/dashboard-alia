<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 24-1-2018
 * Time: 16:13
 */
$func = new Functions();
$tasks = new Tasks();
$ses = new SessionUser();

$categories = $tasks->getServers(true, $ses->getUserID());
?>

<div class="row">
    <?php foreach ($categories as $cat){ ?>
        <?php $allTasks = $tasks->getTasksByCategory(null, $cat['id'], 0) ?>
        <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-yellow">
<!--                    <div class="widget-user-image">-->
<!--                        <img class="img-circle" src="dist/img/no_user2.png" alt="User Avatar">-->
<!--                        <span class="pull-right badge bg-green">--><?php //echo date('d M, Y'); ?><!--</span>-->
<!--                    </div>-->
                    <!-- /.widget-user-image -->
                    <h3 class="widget-user-username"><?php echo $cat['name']; ?></h3>
                    <h5 class="widget-user-desc"><?php echo isset($cat['description']) ? $cat['description'] : '<i>No description</i>' ; ?></h5>
                </div>
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        <li>
                            <a href="<?php echo $func->gotoPage('task_{name}', array('sidebar', 'pages'), null, null, $cat['id']); ?>">Tasks
                                <span class="pull-right badge bg-red"><?php echo sizeof($allTasks); ?></span>
                            </a>
                        </li>
<!--                        <li><a>Completed <span class="pull-right badge bg-red">0</span></a></li>-->
                    </ul>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    <?php } ?>
</div>
