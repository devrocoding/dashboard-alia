<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 24-1-2018
 * Time: 01:22
 */
$user = new User();
$su = new SessionUser();
$role = new Role();
$func = new Functions();
?>

<div class="row">
    <?php foreach ($user->getAllUsers() as $u){?>
    <?php if($u['id']==$su->getUserID())continue; ?>
    <?php $user->selectUser($u['id']); ?>
        <div class="col-md-4">
            <div class="box box-widget widget-user-2">
                <div class="widget-user-header bg-yellow"style="background: url('dist/img/banner.png') -220px 300px;">
                    <div class="widget-user-image">
                        <?php if ($user->getImage() == null){ ?>
                            <img src="dist/img/no_user.png" class="img-circle" alt="User Image">
                        <?php }else{ ?>
                            <img src="dist/img/profile/<?php echo $user->getImage(); ?>" class="img-circle" alt="Image Error">
                        <?php } ?>
                    </div>
                    <!-- /.widget-user-image -->
                    <h1 class="widget-user-username" style="font-weight: bolder; font-size: 40px"><?php echo $user->getNiceName(); ?></h1>
                    <h4 class="widget-user-desc"><?php echo $user->getRole(); ?></h4>
                </div>
                <div class="box-footer no-padding" style="border: 0px">
                    <ul class="nav nav-stacked">
                        <li>
                            <div class="three ui buttons">
                                <a class="ui button" href="<?php echo $func->gotoPage('statitics', array('sidebar', 'pages'), $user->getNiceName(), $user->getUserID()); ?>" style="border-radius: 0px">
                                    <i class="signal icon"></i>
                                    Statitics
                                </a>
                                <a class="ui button <?php echo ($role->canControlOperators($su->getRoleID()) ? '' : 'disabled'); ?>" href="<?php if ($role->canControlOperators($su->getRoleID())) echo $func->gotoPage('profile', array('sidebar', 'pages'), $user->getNiceName(), $user->getUserID()); else echo 'DO NOT TRY THIS M8'; ?>">
                                    <i class="user icon"></i>
                                    Profile
                                </a>
                                <a class="ui button remove_user" user_id="<?php echo $user->getUserID(); ?>" style="border-radius: 0px">
                                    <i class="remove icon"></i>
                                    Remove
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

