<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 21-1-2018
 * Time: 15:01
 */

$func = new Functions();
?>

<ul class="sidebar-menu" data-widget="tree" id="id_searcher">
    <li class="header">Users</li>
    <li class="<?php echo $func->getActiveState('users'); ?>">
        <a href="<?php echo $func->gotoPage('users', array('sidebar', 'pages')); ?>"><i class="fa fa-users"></i> <span>All Users</span></a>
    </li>
    <li class="<?php echo $func->getActiveState('add_user'); ?>">
        <a href="<?php echo $func->gotoPage('add_user', array('sidebar', 'pages')); ?>"><i class="fa fa-user-plus"></i> <span>Add User</span></a>
    </li>
</ul>