<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 21-1-2018
 * Time: 15:01
 */

$func = new Functions();
$user = new SessionUser();
$role = new Role();
?>

<ul class="sidebar-menu" data-widget="tree" id="id_searcher">
    <li class="header">Extra</li>
    <li class="<?php echo $func->getActiveState('statitics'); ?>">
        <a href="<?php echo $func->gotoPage('statitics', array('sidebar', 'pages'), $user->getNiceName(), $user->getUserID()); ?>"><i class="fa fa-info-circle"></i> <span>Your Statitics</span></a>
    </li>
    <?php if ($role->isOperator($user->getRoleID())){ ?>
<!--    <li class="--><?php //echo $func->getActiveState('trello'); ?><!--">-->
<!--        <a href="--><?php //echo $func->gotoPage('trello', array('sidebar', 'pages')); ?><!--"><i class="fa fa-trello"></i> <span>Trello</span></a>-->
<!--    </li>-->
    <?php } ?>
    <?php if ($role->canReportBugs($user->getRoleID())){ ?>
    <li class="<?php echo $func->getActiveState('report_bugs'); ?>">
        <a href="<?php echo $func->gotoPage('report_bugs', array('sidebar', 'pages')); ?>"><i class="fa fa-warning"></i> <span>Report Bugs</span></a>
    </li>
    <?php } ?>
<!--    --><?php //if ($role->canControlStaff($user->getRoleID())){ ?>
<!--    <li class="--><?php //echo $func->getActiveState('announce'); ?><!--">-->
<!--        <a href="--><?php //echo $func->gotoPage('announce', array('sidebar', 'pages')); ?><!--"><i class="fa fa-send"></i> <span>Announce a message</span></a>-->
<!--    </li>-->
<!--    --><?php //} ?>
</ul>
