<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 21-1-2018
 * Time: 15:01
 */

$func = new Functions();
?>

<ul class="sidebar-menu" data-widget="tree" id="id_searcher">
    <li class="header">Admin Statitics</li>
    <li class="<?php echo $func->getActiveState('global_stats'); ?>">
        <a href="<?php echo $func->gotoPage('global_stats', array('sidebar', 'pages')); ?>"><i class="fa fa-trophy"></i> <span>Global User Stats
                <div class="ui purple horizontal label">BETA</div>
            </span></a>
    </li>
    <li class="<?php echo $func->getActiveState('server_stats'); ?>">
        <a href="<?php echo $func->gotoPage('server_stats', array('sidebar', 'pages')); ?>"><i class="fa fa-trophy"></i> <span> Server Stats
            </span></a>
    </li>
<!--    <li class="--><?php //echo $func->getActiveState('task_stats'); ?><!--">-->
<!--        <a href="--><?php //echo $func->gotoPage('add_user', array('sidebar', 'pages')); ?><!--"><i class="fa fa-thumb-tack"></i> <span>Tasks Stats</span></a>-->
<!--    </li>-->
</ul>