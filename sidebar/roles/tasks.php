<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 21-1-2018
 * Time: 15:01
 */

$func = new Functions();
?>

<ul class="sidebar-menu" data-widget="tree" id="id_searcher">
    <li class="header">Tasks</li>
    <li class="<?php echo $func->getActiveState('tasks'); ?>">
        <a href="<?php echo $func->gotoPage('tasks', array('sidebar', 'pages')); ?>"><i class="fa fa-tasks"></i> <span>Your Tasks
            <div class="ui purple horizontal label">BETA</div>
            </span></a>
    </li>
<!--    <li class="--><?php //echo $func->getActiveState('add_task'); ?><!--">-->
<!--        <a href="--><?php //echo $func->gotoPage('add_task', array('sidebar', 'pages')); ?><!--"><i class="fa fa-plus-circle"></i> <span>Add Task</span></a>-->
<!--    </li>-->
</ul>
