<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 19-1-2018
 * Time: 16:00
 */

$user = new SessionUser();
$role = new Role();
?>


<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php if ($user->getImage() == null){ ?>
                    <img src="dist/img/profile/no_user.png" class="user-image" alt="User Image">
                <?php }else{ ?>
                    <img src="<?php echo $user->getImagePath(); ?>" class="user-image ui circular image" alt="User Image">
                <?php } ?>
            </div>
            <div class="pull-left info">
                <p><?php echo $user->getNiceName(); ?></p>
                <!-- Status -->
                <a><i class="fa fa-user"></i><?php echo $user->getRole(); ?></a>
            </div>
        </div>

<!--         search form (Optional)-->
<!--        <form action="#" method="get" class="sidebar-form">-->
<!--            <div class="input-group">-->
<!--                <input type="text" name="q" class="form-control" placeholder="Search...">-->
<!--                <span class="input-group-btn">-->
<!--              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>-->
<!--              </button>-->
<!--            </span>-->
<!--            </div>-->
<!--        </form>-->
<!--         /.search form-->

        <!-- Sidebar Menu -->
<!--        --><?php //if ($role->canControlOperators($user->getRoleID())){ ?>
<!--        <!-- Admin Menu -->
<!--        --><?php //include_once 'roles/admin.php'; ?>
<!--        --><?php //} ?>

        <?php if ($role->canControlStaff($user->getRoleID())){ ?>
            <!-- Users Menu -->
            <?php include_once 'roles/admin.php'; ?>
            <?php include_once 'roles/ha.php'; ?>
        <?php } ?>

        <?php if ($role->isOperator($user->getRoleID())){ ?>
            <?php include_once 'roles/tasks.php'; ?>
        <?php } ?>

        <?php include_once 'roles/extra.php'; ?>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>